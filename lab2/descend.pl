child_of(anna,bridget).
child_of(bridget,caroline).
child_of(caroline,donna).
child_of(donna,emily).

descend(X,Y):- child_of(X,Y).
descend(X,Y):- child_of(X,Z), descend(Z,Y).

?- descend(anna, donna).
