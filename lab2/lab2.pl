%% Exercise 3.2
directly_in(katarina, olga).
directly_in(olga, natasha).
directly_in(natasha, irina).

in(X, Y) :- directly_in(X, Y).
in(X, Y) :-
    directly_in(X, Z),
    in(Z, Y).


%% Exercise 3.3
directTrain(saarbruecken, dudweiler).
directTrain(forbach, saarbruecken).
directTrain(freyming, forbach).
directTrain(stAvold, freyming).
directTrain(fahlquemont, stAvold).
directTrain(metz, fahlquemont).
directTrain(nancy, metz).

travelFromTo(X, Y) :- directTrain(X, Y).
travelFromTo(X, Y) :-
    directTrain(X, Z),
    travelFromTo(Z, Y).


%% Exercise 3.4
greater_than(succ(_), 0).
greater_than(succ(X), succ(Y)) :- greater_than(X, Y).


%% Exercise 3.5
swap(leaf(Label), leaf(Label)).
swap(tree(Left, Right), tree(Nleft, Nright)) :-
    swap(Left, Nright),
    swap(Right, Nleft).


%% Exercise 4
exactly_one_greater_than(succ(0), 0).
exactly_one_greater_than(succ(X), succ(Y)) :- exactly_one_greater_than(X, Y).


%% Exercise 7
travel(Start, End, go(Start, End)) :- directTrain(Start, End).
travel(Start, End, go(Start, Next, Through)) :- directTrain(Start, Next),
                                                travel(Next, End, Through).
