:- module(lab5, [doubled/1, palindrome/1, lastbutone1/2, solve/1]).

:- use_module(utils).

%% Ex 1
doubled(L) :-
    length(L, Len),
    Half is Len / 2,
    slice(L, Half, FirstHalf),
    append(FirstHalf, FirstHalf, L).

%% NOTE better implementation, O(1)
doubledB(L) :- append(Half, Half, L).

%% Ex 2
palindromeL(L) :- reverse(L, L).

%% Ex 3
palindrome(L) :-
    atom_codes(L, CCodes),
    palindromeL(CCodes).

%% Ex 4.1
lastbutone1(L, X) :- reverse(L, [_, X | _]).

%% Ex 4.2
lastbutone2([S, _ | []], S).
lastbutone2([_|T], X) :- lastbutone2(T, X).

%% Ex 5
solve(Puzzle) :-

    %% Triplets of name, age, birthday make up the puzzle's solution
    Puzzle = [[Name1, Age1, Date1],
              [Name2, Age2, Date2],
              [Name3, Age3, Date3],
              [Name4, Age4, Date4]],

    Names = [rosemarie, leticia, nathaniel, edmund],
    Names = [Name1, Name2, Name3, Name4],

    Ages = [5, 6, 8, 18],
    permutation(Ages, [Age1, Age2, Age3, Age4]),

    Dates = [3, 7, 11, 15],
    permutation(Dates, [Date1, Date2, Date3, Date4]),

    %% Rosemarie was born on April 7th
    member([rosemarie, Age1, 7], Puzzle),

    %% Leticia has a birthday 4 days before the 5-year-old
    member([leticia, Age2, Date2], Puzzle),
    member([_, 5, D0], Puzzle),
    Date2 is D0 - 4,

    %% The 18-year-old is either Nathaniel or the one with the April 15th
    %% birthday
    member([N, 18, D], Puzzle),
    (N = Name3,
     D = Date3;
     D = 15),

    %% The 8-year-old has a birthday 4 days after the 6-year-old
    member([_, 8, D1], Puzzle),
    member([_, 6, D2], Puzzle),
    D1 is D2 + 4.
