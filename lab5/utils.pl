:- module(utils, [slice/3, remove/3, permute/2]).

sliceAcc(_, 0, Acc, Res) :- reverse(Acc, Res).
sliceAcc([H|T], N, Acc, Res) :-
    NN is N - 1,
    sliceAcc(T, NN, [H|Acc], Res).

slice(L, S, Ret) :-
    number(S),
    length(L, Len),
    abs(S, SS),
    SS =< Len,
    (S > 0
    ->
    sliceAcc(L, SS, [], Ret)
    ;
    reverse(L, LR),
    sliceAcc(LR, SS, [], Ret)).

remove(X, [X|T], T).
remove(X, [H|T], [H|R]) :- remove(X, T, R).

permute([], []).
permute(L, [H|P]) :-
    remove(H, L, R),
    permute(R, P).
