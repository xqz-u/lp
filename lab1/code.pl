%% ex 2.2
house_elf(dobby).
witch(hermione).
witch('McGonagall').
witch(rita_skeeter).
wizard(ron).

magic(X) :-
    house_elf(X);
    wizard(X);
    witch(X).


%% ex 2.3
word(article, a).
word(article, every).
word(noun, criminal).
word(noun, 'big kahuna burger').
word(verb, eats).
word(verb, likes).

sentence(Word1,  Word2,  Word3, Word4, Word5) :-
    word(article, Word1),
    word(noun, Word2),
    word(verb, Word3),
    word(article, Word4),
    word(noun, Word5).


%% The Music Database
likes(johan, radiohead).
likes(johan, boomtown_rats).
likes(johan, rush).
likes(kees, rem).
likes(kees, kate_bush).
likes(pieter, snow_patrol).
likes(pieter, radiohead).
likes(jan, cranes).
likes(jan, queen).
likes(jan, david_bowie).
likes(marie, kate_bush).
likes(marie, david_bowie).
likes(petra, cranes).
likes(petra, radiohead).

%% difference facts
different(johan, kees).
different(kees, johan).
different(johan, pieter).
different(pieter, johan).
different(johan, jan).
different(jan, johan).
different(johan, marie).
different(marie, johan).
different(johan, petra).
different(petra, johan).
different(kees, pieter).
different(pieter, kees).
different(kees, jan).
different(jan, kees).
different(kees, marie).
different(marie, kees).
different(kees, petra).
different(petra, kees).
different(pieter, jan).
different(jan, pieter).
different(pieter, marie).
different(marie, pieter).
different(pieter, petra).
different(petra, pieter).
different(jan, marie).
different(marie, jan).
different(jan, petra).
different(petra, jan).
different(marie, petra).
different(petra, marie).

%% difference predicate
is_different(X, Y) :-
    different(X, Y);
    different(Y, X).

%% find all different people that like the same music
same_taste(X, Y) :-
    likes(X, Z),
    likes(Y, Z),
    is_different(X, Y).
