sum([H1,H2|L],A,X):-
    H is H1 + H2,
    sum(L,A+H,X).
sum([H|L],A,X):- sum(L,H+A,X).
sum([],A,X):- X is A.

sum(L,S):- sum(L,0,S).
