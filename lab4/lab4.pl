:- module(lab4, [avg/2, fscore/3, house/1, many_houses/1]).

:- use_module(utils, [avg/2]).

%% Ex 3
fscore(P, R, F) :- F is (2 * P * R) / (P + R).

%% Ex 4
print_roof(0, _).
print_roof(K, N) :-
    K > 0,
    M is K - 1,
    O is N + 2,
    tab(M),
    write("/"),
    tab(O),
    write("\\"),
    nl(),
    print_roof(M, O).

draw_roof(K) :-
    print_roof(K, -2).

draw_hor(K, N) :-
    K < N,
    nl().
draw_hor(K, N) :-
    K >= N,
    write("=="),
    M is N + 1,
    draw_hor(K, M).

draw_base(K) :- draw_hor(K, 1).

draw_vert(K, _, N) :- K < N.
draw_vert(K, S, N) :-
    K >= N,
    write("|"),
    tab(S),
    write("|"),
    nl(),
    M is N + 1,
    draw_vert(K, S, M).

draw_walls(K) :-
    S is (K * 2) - 2,
    draw_vert(K, S, 1).

house(K) :-
    draw_roof(K),
    draw_base(K),
    draw_walls(K),
    draw_base(K).

increasing_houses(K, N) :-
    N =< K,
    house(N),
    NN is N + 1,
    increasing_houses(K, NN).

%% wrapper
many_houses(K) :- increasing_houses(K, 1).
