:- module(utils, [add/3, sum/2, avg/2]).

:- use_module(library(apply)).

add(X, Y, Z) :- Z is X + Y.

sum(L, R) :- foldl(add, L, 0, R).

avg(L, R) :-
    length(L, Len),
    sum(L, T),
    R is T / Len.

%% explicitly recursive version of sum/2.
sum_list([], 0).
sum_list([_|T], R) :- R is sum_list(T, R) + 1.
