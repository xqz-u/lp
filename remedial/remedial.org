#+OPTIONS: toc:nil num:nil

#+TITLE: Logic Programming
#+SUBTITLE: Corrections to Lab 3/4
#+AUTHOR: Marco Andrea Gallo (s3680622)
#+DATE: \today

* Corrections Lab 3
  1. Question 4.2.8: the length of the list A = =[[1, 2], [3, 4]|[5, 6, 7]]= is
     not 3, but 5. In fact, the head of list A is a list of lists containing the
     two lists =[1, 2]= and =[3, 4]=; consing it with the list =[5, 6, 7]= gives
     the list =[[1, 2], [3, 4], 5, 6, 7]=, which is clearly of length 5. For A to
     be of length 3, i.e. A = =[[1, 2], [3, 4], [5, 6, 7]]=, it should be defined
     as A = =[[1, 2], [3, 4]|[[5, 6, 7]]]=.
  2. Trees for the =rara/2= queries with variable assignments:
     #+ATTR_LATEX: :placement [!htpb]
     [[./rara_assignments_1.jpg]]

     #+ATTR_LATEX: :placement [!htpb]
     [[./rara_assignments_2.jpg]]
  3. As this question seems to me more of an ought-question than an is question,
     I am not too sure of what the correct answer should be. The
     question would benefit from a clearer rephrasing, specifying that the new
     name must reflect the predicate's function or the likes.
     Nevertheless, I would rename the predicate =rara/2= to something like
     =middle_odd_l/2=, since it only succeeds with lists of odd length greater
     than 1 and unifies the result variable with the list's middle element.

* Corrections Lab 4
  Updated search tree for the query =sum([3, 2, 5], Sum)=:
  #+ATTR_LATEX: :placement [!htpb]
  [[./sum_tree_complete.jpg]]
