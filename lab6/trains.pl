%% LPL 10.4

directTrain(saarbruecken, dudweiler).
directTrain(forbach, saarbruecken).
directTrain(freyming, forbach).
directTrain(stAvold, freyming).
directTrain(fahlquemont, stAvold).
directTrain(metz, fahlquemont).
directTrain(nancy, metz).

%% there exist a direct connection between A and B whenever there is a direct
%% train A -> B or a direct train B -> A.
direct(A, B) :-
    directTrain(A, B);
    directTrain(B, A).


%% base case
%% NOTE not cutting at the end or other possible paths would be unexplored
route(End, End, Visited, Route) :-  reverse(Visited, Route).

%% proceed depth first to next location, fail and backtrack when reaching dead
%% end. keep track of visited stations to avoid infinite recursion between dead
%% end and previous station because of direct/2.
route(Start, End, Visited, Route) :-
    direct(Start, Next),
    \+ member(Next, Visited),
    route(Next, End, [Next|Visited], Route).


%% wrapper adding starting station to visited stations
route(A, B, R) :-  route(A, B, [A], R).
