likes(johan,radiohead).
likes(johan,boomtown_rats).
likes(johan,rush).
likes(kees,rem).
likes(kees,kate_bush).
likes(pieter,snow_patrol).
likes(pieter,radiohead).
likes(jan,cranes).
likes(jan,queen).
likes(jan,david_bowie).
likes(marie,kate_bush).
likes(marie,david_bowie).
likes(silvia,cranes).
likes(silvia,radiohead).

same_taste(X, Y) :-
    likes(X, Z),
    likes(Y, Z),
    \+(X = Y).
