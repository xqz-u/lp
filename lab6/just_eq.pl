%% find location and magnitude of hardest earthquake, aka that `eq` fact with
%% magnitude Magn s.t. there exist no other magnitude Magn1 with Magn1 > Magn
hardest_eq(Loc, Magn) :-
    eq(_, _, Magn, Loc, _, _),
    \+ (eq(_, _, Magn1, _, _, _),
        Magn1 > Magn).
