%% LPL 10.3

split([], [], []).
split([X|XS], [X|P], N) :-
    X >= 0,
    split(XS, P, N).
split([X|XS], P, [X|N]) :-
    X < 0,
    split(XS, P, N).


%% the ! will prevent backtracking from trying other branches
split_c([], [], []).
split_c([X|XS], [X|P], N) :-
    X >= 0,
    !,
    split(XS, P, N).
split_c([X|XS], P, [X|N]) :-
    X < 0,
    !,
    split(XS, P, N).
