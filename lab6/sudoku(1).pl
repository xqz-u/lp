%
% sudoku.pl by Johan Bos (12/12/2020)
%

/* -------------------------------------------
   Define the sudoku that needs to be solved
------------------------------------------- */

sudoku(Sudoku):-

    Sudoku = [ [ _,_,_, 1,_,_, 8,5,_],
               [ 7,_,_, _,_,_, _,_,9],
               [ _,1,8, _,3,_, 7,4,_],

               [ _,4,_, _,_,_, _,_,2],
               [ _,_,_, _,_,5, _,_,_],
               [ _,7,_, _,_,4, 1,_,_],

               [ _,_,_, _,_,2, _,_,_],
               [ 1,_,_, 5,_,3, 9,8,_],
               [ _,8,4, 9,7,_, _,_,5] ].


/* -------------------------------------------
   Printing a sudoko on the screen
------------------------------------------- */

print_sudoku([]):- print_bar.
print_sudoku([X,Y,Z|L]):-
    print_bar,
    write(' |'), print_line(X),
    write(' |'), print_line(Y),
    write(' |'), print_line(Z),
    print_sudoku(L).

print_line([]):- nl.
print_line([X,Y,Z|L]):- print_cell(X), print_cell(Y), print_cell(Z), write('|'), print_line(L).

print_cell(X):- var(X), !, write(' _ ').
print_cell(X):- number(X), !, write(' '), write(X), write(' ').

print_bar:- write('  -----------------------------'), nl.


/* -------------------------------------------
   Solving the sudoku (generate-and-test)
------------------------------------------- */

solve(Su):-
    \+ (member(Row,Su), member(X,Row), var(X)), !.
solve(Su):-
    fill_one(Su),
    check_rows(Su), check_columns(Su), check_blocks(Su),
    solve(Su).


/* -------------------------------------------
   Check whether all numbers are different
------------------------------------------- */

check_rows([]).
check_rows([Row|L]):-
    check(Row,[]),
    check_rows(L).

check_columns([[],[],[],[],[],[],[],[],[]]).
check_columns([[H1|T1],[H2|T2],[H3|T3],[H4|T4],[H5|T5],[H6|T6],[H7|T7],[H8|T8],[H9|T9]]):-
    check([H1,H2,H3,H4,H5,H6,H7,H8,H9],[]),
    check_columns([T1,T2,T3,T4,T5,T6,T7,T8,T9]).

check_blocks([]).
check_blocks([[],[],[]|T]):- check_blocks(T).
check_blocks([[H1,H2,H3|T1],[H4,H5,H6|T2],[H7,H8,H9|T3]|T]):-
    check([H1,H2,H3,H4,H5,H6,H7,H8,H9],[]),
    check_blocks([T1,T2,T3|T]).

check([],_).
check([X|L],Acc):- var(X), !, check(L,Acc).
check([X|L],Acc):- number(X), \+ member(X,Acc), check(L,[X|Acc]).


/* -------------------------------------------
   Fill a cell with a number
------------------------------------------- */

fill_one(Sudoku):-
    member(Row,Sudoku),
    member(X,Row), var(X), !,
    member(X,[1,2,3,4,5,6,7,8,9]).


/* -------------------------------------------
   Wrapper
------------------------------------------- */

solve:- sudoku(S), print_sudoku(S), solve(S), print_sudoku(S).

%% :- solve.
