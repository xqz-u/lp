rara([X|L], A, M) :- rara(L, [Y|A], M).
rara([M|L], L, [M]).

rara(L, M) :- rara(L, [], M).

?- rara([a,b,c,d],X).
?- rara([1,2,3],X).
?- rara([oh,no],Y).
?- rara([],X).
