* Assignment week 3
1. Exercises 4.2 (lists), 4.4 (swap/2), 4.5 (translation), 4.6 (twice).
2. Write a predicate =second_third/2=, where the first argument is a list =L=,
   and the second argument is a list of two elements containing the second and
   third member of =L=. For instance:

   #+BEGIN_SRC: prolog
   ?- second_third([a,b,c,d,e],X).
   X=[b,c]
   yes
   #+END_SRC
3. Use the definition of =member/2= given in Chapter 4 of LPN. Give the
   exhaustive search trees for the following four queries:

   #+BEGIN_SRC: prolog
   ?- member(M,[a,p(x)]).
   ?- member(p(X),[q(a),p(a)]).
   ?- member(P,[[]]).
   ?- member(x,[a,b,c]).
   #+END_SRC
4. Consider the following Prolog database:

   #+BEGIN_SRC: prolog
   rara([X|L],A,M):- rara(L,[Y|A],M).
   rara([M|L],L,[M]).
   rara(L,M):- rara(L,[],M).
   #+END_SRC

   How many predicates are defined?
5. Draw exhaustive search trees for the following queries:

   #+BEGIN_SRC: prolog
   ?- rara([a,b,c,d],X).
   ?- rara([1,2,3],X).
   ?- rara([oh,no],Y).
   ?- rara([],X).
   #+END_SRC
   What would be a better name for this predicate?
