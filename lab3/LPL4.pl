%% Exercise 4.4
swap12(List1, List2) :-
    List1 = [One, Two|T],
    List2 = [Two, One|T].

%% Exercise 4.5
tran(eins, one).
tran(zwei, two).
tran(drei, three).
tran(vier, four).
tran(fuenf, five).
tran(sechs, six).
tran(sieben, seven).
tran(acht, eight).
tran(neun, nine).

listtran([], []).
listtran([G|T1], [E|T2]) :-
    tran(G, E),
    listtran(T1, T2).

%% Exercise 4.6
twice([], []).
twice([H|T1], [H, H| T2]) :- twice(T1, T2).
