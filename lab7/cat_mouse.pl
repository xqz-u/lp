:- dynamic maze/3.
:- dynamic cat/3.
:- dynamic mouse/3.

maze(['┏━┳━━━━━━━━━━━━━━━━━┓',
      '┃ ┃                 ┃',
      '┃ ┣━━━━━━━━━━━━━━━┫ ┃',
      '┃ ┃                 ┃',
      '┃ ┃ ┃ ┣━━━━━━━━━━━━━┫',
      '┃   ┃               ┃',
      '┣━━━╋━━━┓ ┏━┳━━━┓ ┏━┫',
      '┏━┳━━━━━━━━━━━━━━━━━┓']).
      %% '┃   ┃   ┃ ┃ ┃   ┃ ┃ ┃',
      %% '┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃',
      %% '┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃',
      %% '┃ ┃ ┗━┛ ┃ ┃   ┃ ┃ ┃ ┃',
      %% '┃ ┃     ┃ ┃ ┃ ┃ ┃ ┃ ┃',
      %% '┃ ┗━━━━━┛ ┃ ┃ ┃ ┃ ┃ ┃',
      %% '┃         ┃ ┃ ┃ ┃ ┃ ┃',
      %% '┣━━━━━┓ ━━┻━┫ ┃ ┃ ┃ ┃',
      %% '┃     ┃     ┃ ┃   ┃ ┃',
      %% '┣━━━━ ┗━━━━━┛ ┗━━━┛ ┃',
      %% '┃                   ┃',
      %% '┗━━━━━━━━━━━━━━━━━━━┛']).

init(1):-
   retractall(cat(_,_,_)),
   retractall(mouse(_,_,_)),
   init_position(CatX,CatY),
   init_position(MouseX,MouseY),
   asserta(cat(1,CatX,CatY)),
   asserta(mouse(1,MouseX,MouseY)).

init_position(X,Y):- X is random(20), Y is random(20), maze(X,Y,32), !.
init_position(X,Y):- init_position(X,Y).

create_maze:- maze(_,_,_), !, retractall(maze(_,_,_)), create_maze.
create_maze:- maze(L), create_maze(L,0).

create_maze([],_).
create_maze([Row|L],OldY):-
   Y is OldY + 1,
   atom_codes(Row,Codes),
   create_row(Codes,0,Y),
   create_maze(L,Y).

create_row([],_,_).
create_row([C|L],OldX,Y):- X is OldX + 1, assert(maze(X,Y,C)), create_row(L,X,Y).

display_maze(N):-
   clean,                 % clean the screen
   display_maze(0,N),     % display the maze starting with row 0
   sleep(0.1).            % take a brief pause

display_maze(OldY,_):-
   Y is OldY + 1,         % the next row is not part
   \+ maze(_,Y,_), !.     % of the maze so we are ready

display_maze(OldY,N):-
   Y is OldY + 1,
   tab(5),                % space indentation
   display_maze(0,Y,N),   % display the row starting with column 0
   nl,                    % add a new line
   display_maze(Y,N).     % display all other rows

display_maze(OldX,Y,_):- X is OldX + 1, \+ maze(X,Y,_), !.
display_maze(OldX,Y,N):- X is OldX + 1, cat(N,X,Y), !,    format('~w',['C']), display_maze(X,Y,N).
display_maze(OldX,Y,N):- X is OldX + 1, cat(P,X,Y), P is N-1, !,    format('~w',['▪']), display_maze(X,Y,N).
display_maze(OldX,Y,N):- X is OldX + 1, cat(P,X,Y), P is N-2, !,    format('~w',['▪']), display_maze(X,Y,N).
display_maze(OldX,Y,N):- X is OldX + 1, cat(P,X,Y), P is N-3, !,    format('~w',['•']), display_maze(X,Y,N).
display_maze(OldX,Y,N):- X is OldX + 1, mouse(N,X,Y), !,  format('~w',['M']), display_maze(X,Y,N).
display_maze(OldX,Y,N):- X is OldX + 1, mouse(P,X,Y), P is N-1, !,    format('~w',['•']), display_maze(X,Y,N).
display_maze(OldX,Y,N):- X is OldX + 1, maze(X,Y,Z), !, format('~c',[Z]),   display_maze(X,Y,N).

clean:-
   format('~n~n~n~n~n~n~n~n~n~n~n~n~n~n~n~n',[]),
   format('~n~n~n~n~n~n~n~n~n~n~n~n~n~n~n~n',[]),
   format('~n~n~n~n~n~n~n~n~n~n~n~n~n~n~n~n',[]).

move(Now):-
    display_maze(Now),
    Next is Now + 1,
    move_mouse(Next),
    move_cat(Next),
    move(Next).

move_cat(Next):-
   Current is Next-1,
   cat(Current,X,Y),
   findall(pos(C,D),(new_pos(pos(X,Y),pos(C,D)), \+ cat(_,C,D)),L),
   length(L,N), N > 0, !,
   Choice is random(N)+1,
   nth1(Choice,L,pos(A,B)),
   asserta(cat(Next,A,B)).

move_cat(Next):-
   Current is Next-1,
   cat(Current,X,Y),
   findall(pos(C,D),new_pos(pos(X,Y),pos(C,D)),L),
   member(pos(A,B),L),
   cat(N,A,B),
   \+ (member(pos(A1,B1),L), cat(N1,A1,B1), N1 < N), !,
   retractall(cat(_,A,B)),
   asserta(cat(Next,A,B)).

move_mouse(Next):-
   Current is Next-1,
   mouse(Current,X,Y),
   findall(pos(C,D),(new_pos(pos(X,Y),pos(C,D)), \+ mouse(_,C,D)),L),
   length(L,N), N > 0, !,
   Choice is random(N)+1,
   nth1(Choice,L,pos(A,B)),
   asserta(mouse(Next,A,B)).

move_mouse(Next):-
   Current is Next-1,
   mouse(Current,X,Y),
   findall(pos(C,D),new_pos(pos(X,Y),pos(C,D)),L),
   member(pos(A,B),L),
   mouse(N,A,B),
   \+ (member(pos(A1,B1),L), mouse(N1,A1,B1), N1 < N), !,
   retractall(mouse(_,A,B)),
   asserta(mouse(Next,A,B)).

new_pos(pos(X,Y),pos(NewX,Y)):- NewX is X + 1, maze(NewX,Y,32).
new_pos(pos(X,Y),pos(NewX,Y)):- NewX is X - 1, maze(NewX,Y,32).
new_pos(pos(X,Y),pos(X,NewY)):- NewY is Y + 1, maze(X,NewY,32).
new_pos(pos(X,Y),pos(X,NewY)):- NewY is Y - 1, maze(X,NewY,32).

start:- create_maze, init(N), move(N).
