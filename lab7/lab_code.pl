%% Ex 1.2
display_maze(OldX,Y,N):- X is OldX + 1, mouse(P,X,Y), P is N-2, !,    format('~w',['•']), display_maze(X,Y,N).

%% Ex 1.3
display_maze(N):-
    clean,                 % clean the screen
    display_maze(0,N),     % display the maze starting with row 0
    sleep(0.05).           % take a brief pause (MODIFIED)

%% Ex 1.4
move(Now):-
    display_maze(Now),
    Next is Now + 1,
    move_mouse(Next),
    move_cat(Next),
    (cat(T,X,Y),
     mouse(T,X,Y),
     !;
     move(Next)).

%% Ex 2.1
print_exists(Smt, Fmt) :-
    \+ Smt = [],
    format(Fmt, [Smt]);
    true.

% predicate for displaying the movements of the farmer
print_mv([T, Shore, Boat, Island]) :-
    writeln(T),
    %% UNCOMMENT THESE TO HIDE EMPTY LISTS
    %% print_exists(Shore, "\t\tShore: ~w\n"),
    %% print_exists(Boat, "\t\tBoat: ~w\n"),
    %% print_exists(Island, "\t\tIsland: ~w\n").
    format("\t\tShore: ~w\n", [Shore]),
    format("\t\tBoat: ~w\n", [Boat]),
    format("\t\tIsland: ~w\n", [Island]).

display :-
    setof([Time,Shore,Boat,Island], state(Time,Shore,Boat,Island), L),
    forall(member(Line, L), print_mv(Line)).
