:- dynamic state/4.

print_exists(Smt, Fmt) :-
    \+ Smt = [],
    format(Fmt, [Smt]);
    true.

% predicate for displaying the movements of the farmer
print_mv([T, Shore, Boat, Island]) :-
    writeln(T),
    %% UNCOMMENT THESE TO HIDE EMPTY LISTS
    %% print_exists(Shore, "\t\tShore: ~w\n"),
    %% print_exists(Boat, "\t\tBoat: ~w\n"),
    %% print_exists(Island, "\t\tIsland: ~w\n").
    format("\t\tShore: ~w\n", [Shore]),
    format("\t\tBoat: ~w\n", [Boat]),
    format("\t\tIsland: ~w\n", [Island]).

display :-
    setof([Time,Shore,Boat,Island], state(Time,Shore,Boat,Island), L),
    forall(member(Line, L), print_mv(Line)).

% wrapper doing the initalisation and starting the action
%
farmer:-
   Shore = [cabbage,farmer,goat,wolf],
   Boat = [],
   Island = [],
   Time = 1,
   State = state(Time,Shore,Boat,Island),
   retractall(state(_,_,_,_)),
   assert(State),
   action(Shore,Boat,Island,1).

% the result of a new actions is if the farmer is present or
% if the farmer is not present and it is not the case that
% the goat and the cabbage are together and it is not the
% case that the wolf and the goat are not together
%
safe(L):- member(farmer,L), !.
safe(L):-
    \+ member(farmer, L),
    !,
    \+ (member(goat,L), member(cabbage,L)),
    \+ (member(wolf,L), member(goat,L)).
%% safe(L):-
%%     \+ (member(goat,L), member(cabbage,L)),
%%     \+ (member(wolf,L), member(goat,L)).


% the boat can contain nothing, or just the farmer,
% something and the farmer, or the farmer and something
%
boat([]).
boat([farmer]).
boat([_,farmer]).
boat([farmer,_]).

% the shore and boat are empty, this means all the
% other items are on the island
%
action([],[],_,_):-
    !, display.

% move items from the shore to the boat
%
action(Shore,Boat,Island,Time):-
   move(Shore,Boat,NewShore,NewBoat),
   State = state(NewTime,NewShore,NewBoat,Island),
   \+ State,          % have not been before in this state
   safe(NewShore),    % this is safe to do
   boat(NewBoat),     % boat not overloaded
   !,
   NewTime is Time + 1,
   assert( State ),
   action(NewShore,NewBoat,Island,NewTime).

% move items from the boat to the island
%
action(Shore,Boat,Island,Time):-
   move(Boat,Island,NewBoat,NewIsland),
   State = state(NewTime,Shore,NewBoat,NewIsland),
   \+ State,
   safe(NewIsland),
   boat(NewBoat),
   !,
   NewTime is Time + 1,
   assert( State ),
   action(Shore,NewBoat,NewIsland,NewTime).

% move just the farmer from A to B or vice versa
% use built-in sort/2 to get items in canonical order
%
move(A1,B1,A2,B2):- select(farmer,A1,A2), sort([farmer|B1],B2).
move(A1,B1,A2,B2):- select(farmer,B1,B2), sort([farmer|A1],A2).

% move the farmer and something else from A to B or vice versa
% use built-in sort/2 to get items in canonical order
%
move(A1,B1,A3,B2):- select(farmer,A1,A2), select(X,A2,A3), sort([farmer,X|B1],B2).
move(A1,B1,A2,B3):- select(farmer,B1,B2), select(X,B2,B3), sort([farmer,X|A1],A2).
