#+OPTIONS: toc:nil num:nil

#+TITLE: Logic Programming
#+SUBTITLE: Assignment 7
#+AUTHOR: Marco Andrea Gallo (s3680622)
#+DATE: 15 January 2021

* Exercise 1
  1. The file =cat_mouse.pl= defines three dynamic predicates, namely =maze/3=,
     =cat/3= and =mouse/3=. All the other predicates used are static.
  2.
     #+ATTR_LATEX: :options frame=single
     #+BEGIN_SRC prolog
       display_maze(OldX,Y,N) :-
           X is OldX + 1,
           mouse(P,X,Y),
           P is N-2, !,
           format('~w',['•']),
           display_maze(X,Y,N).
     #+END_SRC
  3.
     #+ATTR_LATEX: :options frame=single
     #+BEGIN_SRC prolog
       display_maze(N):-
           clean,
           display_maze(0,N),
           sleep(0.05).
     #+END_SRC
  4.
     #+ATTR_LATEX: :options frame=single
     #+BEGIN_SRC prolog
move(Now):-
    display_maze(Now),
    Next is Now + 1,
    move_mouse(Next),
    move_cat(Next),
    (cat(T,X,Y),
     mouse(T,X,Y),
     !;
     move(Next)).
     #+END_SRC

* Exercise 2
  1.
      #+ATTR_LATEX: :options frame=single
      #+BEGIN_SRC prolog
 print_exists(Smt, Fmt) :-
     \+ Smt = [],
     format(Fmt, [Smt]);
     true.

 % predicate for displaying the movements of the farmer
 print_mv([T, Shore, Boat, Island]) :-
     writeln(T),
     %% UNCOMMENT THESE TO HIDE EMPTY LISTS
     %% print_exists(Shore, "\t\tShore: ~w\n"),
     %% print_exists(Boat, "\t\tBoat: ~w\n"),
     %% print_exists(Island, "\t\tIsland: ~w\n").
     format("\t\tShore: ~w\n", [Shore]),
     format("\t\tBoat: ~w\n", [Boat]),
     format("\t\tIsland: ~w\n", [Island]).

 display :-
     setof([Time,Shore,Boat,Island], state(Time,Shore,Boat,Island), L),
     forall(member(Line, L), print_mv(Line)).
      #+END_SRC
  2.
      #+ATTR_LATEX: :options frame=single
      #+BEGIN_SRC prolog
safe(L):- member(farmer,L), !.
safe(L):-
    \+ member(farmer, L),
    !,
    \+ (member(goat,L), member(cabbage,L)),
    \+ (member(wolf,L), member(goat,L)).
      #+END_SRC
